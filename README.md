# Grump.js

https://twitter.com/landongn/status/1008482403269005312
>you've used grunt, you've built a gulpfile, you've run yarn and parcel and webpack, but now, today, I give you the logical conclusion of tooling fatigue: introducing grump.js

https://twitter.com/intothemild/status/1008613974471045120
>Back in my day we just zipped everything and uploaded it to the ftp. And we were HAPPY


---
## About

Grump just aliases zip.. but way more overcomplicated because javascript

## Usage
```sh
grump <file/directory>
```

That's it