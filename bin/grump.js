#! /usr/bin/env node
const program = require('commander');
const grump = require("../lib");

program
    .arguments('<file>')
    .action(grump, program)
    .parse(process.argv);
