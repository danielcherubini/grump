const fs = require("fs");
const path = require("path");
const walk = require("walk");
var zip = new require('node-zip')();

function grump(file) {
    const cwd = process.cwd();
    const joinedPath = path.join(cwd, file);
    const parsedPath = path.parse(joinedPath);
    const stats = fs.statSync(joinedPath);
    
    if (stats.isDirectory()) {
        grumpDirectory(joinedPath);
    } else if (stats.isFile()) {
        grumpFile(joinedPath);
    }
}

function grumpDirectory(directory) {
    let files = [];
    const walker = walk.walk(directory, { followLinks: false });
    walker
        .on("file", function(root, stat, next) {
            const filename = path.join(root, stat.name);
            addFile(filename);
            next();
        })
        .on('end', function() {
            doZip()
        });
}

function grumpFile(file) {
    addFile(file);
    doZip();
}

function addFile(filename) {
    const fileContent = fs.readFileSync(filename);
    zip.file(filename.replace(process.cwd() + "/", ""), fileContent);
}

function doZip() {
    const data = zip.generate({base64:false,compression:'DEFLATE'});
    printGrumpyMessage();
    fs.writeFileSync('grump.zip', data, 'binary');
}

function printGrumpyMessage() {
    const messages = [
        "Grumping your files... honestly why even bother",
        "Back in my day we just uploaded the files to the ftp and it was good",
        "FUCK IT WE'LL DO IT LIVE"
    ]
    const message = messages[Math.floor(Math.random()*messages.length)];
    console.log(message);
}

module.exports = grump;